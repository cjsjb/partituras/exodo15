	\context ChordNames
		\chords {
		\set Score.markFormatter = #format-mark-box-numbers

		% intro
		\skip 1*2

		% cantemos al sennor...
		\skip 1*10

		% cantad a yahve...
		\mark \default
		\skip 1*9

		% cantemos al sennor...
		\skip 1*10

		% el es mi dios...
		\mark \default
		\skip 1*9

		% cantemos al sennor...
		\skip 1*10

		% los carros y jinetes...
		\mark \default
		\skip 1*9

		% cantemos al sennor...
		\skip 1*10

		% tu diestra, sennor...
		\mark \default
		\skip 1*9

		% cantemos al sennor...
		\skip 1*10

		% quien como tu...
		\mark \default
		\skip 1*9
		}

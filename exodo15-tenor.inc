\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key d \major

		R1  |
		r2. a 4  |
		d 4 e fis g  |
		a 2 r4 a  |
%% 5
		b 4 a g b  |
		a 2 a 4 -\staccato d'  |
		d' 4 d' d' d'  |
		cis' 2 r4 cis'  |
		d' 4 cis' b a  |
%% 10
		a 1  |
		fis 1  |
		r2 r4. a 8  |
		\key d \minor
		bes 4 g 8 g c' 4. bes 8  |
		a 8 bes a g f f 4 -\staccato f 8  |
%% 15
		g 8 g g g a a a g  |
		fis 8 g 4 a a 8 a a  |
		bes 8 bes bes g c' c' 4 r8  |
		a 8 ( bes a ) g f 4 r8 f  |
		g 4 g g g  |
%% 20
		\key d \major
		a 1 ~  |
		a 2 r4 a  |
		d 4 e fis g  |
		a 2 r4 a  |
		b 4 a g b  |
%% 25
		a 2 a 4 -\staccato d'  |
		d' 4 d' d' d'  |
		cis' 2 r4 cis'  |
		d' 4 cis' b a  |
		a 1  |
%% 30
		fis 1  |
		r2 r4 r8 a  |
		\key d \minor
		bes 4 g c' 4. r8  |
		a 8 bes a g f 4 f 8 f  |
		g 4 g 8 g a a 4.  |
%% 35
		fis 8 g fis g a 4 a 8 a  |
		bes 8 bes bes g c' c' 4 bes 8  |
		a 8 bes a g f 4 r8 f  |
		g 4 g 8 g g 4 g  |
		\key d \major
		a 1 ~  |
%% 40
		a 2 r4 a  |
		d 4 e fis g  |
		a 2 r4 a  |
		b 4 a g b  |
		a 2 a 4 -\staccato d'  |
%% 45
		d' 4 d' d' d'  |
		cis' 2 r4 cis'  |
		d' 4 cis' b a  |
		a 1  |
		fis 1  |
%% 50
		r2 r4 r8 a  |
		\key d \minor
		bes 8 bes bes g c' c' 4 bes 8  |
		a 8 bes a g f f 4 -\staccato f 8  |
		g 8 g g g a a a g  |
		fis 8 g 4 a 4. r8 a  |
%% 55
		bes 8 bes bes g c' c' 4 bes 8  |
		a 8 bes a g f f r f  |
		g 4 g g g  |
		\key d \major
		a 1 ~  |
		a 2 r4 a  |
%% 60
		d 4 e fis g  |
		a 2 r4 a  |
		b 4 a g b  |
		a 2 a 4 -\staccato d'  |
		d' 4 d' d' d'  |
%% 65
		cis' 2 r4 cis'  |
		d' 4 cis' b a  |
		a 1  |
		fis 1  |
		r2 r4 r8 a  |
%% 70
		\key d \minor
		bes 4 g 8 g c' 4. bes 8  |
		a 8 bes a g f f 4 -\staccato f 8  |
		g 4 g 8 g a 4. g 8  |
		fis 8 g fis g a a r a  |
		bes 8 bes bes g c' 4 c' 8 bes  |
%% 75
		a 8 bes a g f f r4  |
		g 4 g g g  |
		\key d \major
		a 2 a ~  |
		a 2 r4 a  |
		d 4 e fis g  |
%% 80
		a 2 r4 a  |
		b 4 a g b  |
		a 2 a 4 -\staccato d'  |
		d' 4 d' d' d'  |
		cis' 2 r4 cis'  |
%% 85
		d' 4 cis' b a  |
		a 1  |
		fis 1  |
		R1  |
		\key d \minor
		bes 4 g 8 g c' 4. bes 8  |
%% 90
		a 8 bes a g f f 4 -\staccato r8  |
		g 4 g 8 g a 4. g 8  |
		fis 8 g fis g a 4 r8 a  |
		bes 8 bes bes g c' c' 4 bes 8  |
		a 8 bes a g f f r f  |
%% 95
		g 4 g 8 g g 4 g  |
		\key d \major
		a 1 ~  |
		a 2 r4 a  |
		d 4 e fis g  |
		a 2 r4 a  |
%% 100
		b 4 a g b  |
		a 2 a 4 -\staccato d'  |
		d' 4 d' d' d'  |
		cis' 2 r4 cis'  |
		d' 4 cis' b a  |
%% 105
		a 1  |
		fis 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.

		Can -- tad a Yah -- vé, pues se cu -- brió de glo -- ria:
		los ca -- rros y ca -- ba -- llos a -- rro -- "jó en" el mar.
		Mi for -- ta -- le -- za y mi -- can -- to es __ Yah -- vé,
		Él es mi sal -- va -- ción. __

		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.

		Él es mi Dios, yo "le a" -- la -- ba -- ré;
		es el Dios de mis pa -- dres, yo le can -- ta -- ré.
		El Se -- ñor es un gue -- rre -- ro, su nom -- bre es Yah -- vé,
		Él es nues -- tra sal -- va -- ción. __

		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.

		Los ca -- rros y ji -- ne -- tes del fa -- ra -- ón "de E" -- gip -- to;
		la flor de sus gue -- rre -- ros a -- rro -- "jó en" el mar;
		ca -- ye -- ron has -- "ta el" fon -- do, las o -- las los cu -- brie -- ron;
		qué gran -- de es Yah -- vé. __

		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.

		Tu dies -- tra, Se -- ñor, re -- lum -- bra por su fuer -- za;
		tu dies -- tra, Se -- ñor, de -- rro -- "ta al" e -- ne -- mi -- go.
		Has guia -- do por "tu a" -- mor al pue -- blo que res -- ca -- tas -- te,
		has -- ta tu mo -- ra -- da. __

		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.

		¿Quién co -- mo Tú, Yah -- vé, en -- tre los gran -- des?
		¿Quién co -- mo Tú, su -- bli -- "me en" san -- ti -- dad?
		Gran -- dio -- so en pro -- di -- gios, au -- tor de ma -- ra -- vi -- llas.
		¡Tú e -- res el sal -- va -- dor! __

		% estribillo
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
		Can -- te -- mos al Se -- ñor,
		su -- bli -- "me es" su vic -- to -- ria.
	}
>>

\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		a1 a1

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1

		% cantad a yahve...
		g2:m c2 f2 d2:m
		g2:m a2:7 d2 d2:7
		g2:m c2 f2 d2:m
		g2:m e2:m a1 a1:7

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1

		% el es mi dios...
		g2:m c2 f2 d2:m
		g2:m a2:7 d2 d2:7
		g2:m c2 f2 d2:m
		g2:m e2:m a1 a1:7

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1

		% los carros y jinetes...
		g2:m c2 f2 d2:m
		g2:m a2:7 d2 d2:7
		g2:m c2 f2 d2:m
		g2:m e2:m a1 a1:7

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1

		% tu diestra, sennor...
		g2:m c2 f2 d2:m
		g2:m a2:7 d2 d2:7
		g2:m c2 f2 d2:m
		g2:m e2:m a1 a1:7

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1

		% quien como tu...
		g2:m c2 f2 d2:m
		g2:m a2:7 d2 d2:7
		g2:m c2 f2 d2:m
		g2:m e2:m a1 a1:7

		% cantemos al sennor...
		d1 fis1:m g1 a1
		d1 fis1:m g2 e2:m a1 d1
		R1
	}
